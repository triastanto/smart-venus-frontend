<?php

namespace App\Http\Controllers;

class DocumentController extends Controller
{
    public function index()
    {
        return view('document.index');
    }

    public function create()
    {
        return view('document.create');
    }

    public function show()
    {
        return view('document.show');
    }
}
