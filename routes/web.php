<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'document', 'as' => 'document.'], function () {
    Route::get('', 'DocumentController@index')->name('index');
    Route::get('create', 'DocumentController@create')->name('create');
    Route::get('show', 'DocumentController@show')->name('show');
    Route::get('edit', 'DocumentController@edit')->name('edit');
});
