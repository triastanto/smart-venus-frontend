<div class="form-group">
  <div class="custom-file mt-2">
    <input id="inputGroupFile04" type="file" multiple class="custom-file-input" multiple>
    <label class="custom-file-label" for="inputGroupFile04">
      <span class="d-inline-block text-truncate w-75">Pilih beberapa file</span>
    </label>
  </div>
  <p class="help-block">Total maksimal upload 32MB</p>
</div>
