<div class="row">
    <div class="col-3">
        <div class="form-group">
            <input class="form-control" type="text" placeholder="Pilih tanggal surat" />
        </div>
    </div>
    <div class="col-9">
        <div class="form-group">
            <select class="form-control select2" data-placeholder="Pilih asal surat" data-allow-clear="true"
                style="width: 100%;">
                <option></option>
                <option value="10">Divisi Business Enabler & ICT</option>
                <option value="20">Dinas ICT & Support</option>
            </select>
        </div>
    </div>
</div>
<div class="form-group select2-info">
    <select class="select2" multiple="multiple" data-placeholder="Pilih penerima surat"
        data-dropdown-css-class="select2-info" style="width: 100%;">
        <option>Manager Quality Control</option>
        <option>Specialist Analytical Application</option>
        <option>Abdul Hadi Muslim (10083)</option>
    </select>
</div>
<div class="form-group">
    <select class="select2" multiple="multiple" data-placeholder="Pilih penerima tembusan" style="width: 100%;">
        <option>Manager Management Accounting</option>
        <option>Sr Specialist End User Device & Network</option>
        <option>Asep Saepudin Usman (9991)</option>
    </select>
</div>
<div class="row">
    <div class="col-3">
        <div class="form-group">
            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                <label class="btn btn-default active" title="Biasa">
                    <input type="radio" name="options" id="option1" autocomplete="off" checked> <i class="far fa-comment-dots text-info"></i>
                </label>
                <label class="btn btn-default" title="Penting">
                    <input type="radio" name="options" id="option2" autocomplete="off"> <i class="fas fa-exclamation text-warning"></i>
                </label>
                <label class="btn btn-default" title="Rahasia">
                    <input type="radio" name="options" id="option3" autocomplete="off"> <i class="fas fa-lock text-danger"></i>
                </label>
            </div>
        </div>
    </div>
    <div class="col-3">
        <div class="form-group">
            <select class="form-control select2" data-placeholder="Pilih jenis surat" data-allow-clear="true"
                style="width: 100%;">
                <option></option>
                <option value="10">Memo Dinas</option>
                <option value="20">Edaran</option>
                <option value="20">Undangan</option>
                <option value="20">Surat</option>
            </select>
        </div>
    </div>
    <div class="col-6">
        <div class="form-group">
            <input class="form-control" placeholder="Isi subyek surat">
        </div>
    </div>
</div>