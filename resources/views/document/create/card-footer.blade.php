<div class="float-right">
    <button type="button" class="btn btn-default" data-alert-type="info"
        data-alert-text="Apakah Anda yakin surat disimpan sebagai draf?">
        <i class="fas fa-pencil-alt"></i> Draf
    </button>
    <button type="submit" class="btn btn-primary" data-alert-type="info"
        data-alert-text="Apakah Anda yakin surat dikirim kepada pemaraf/penerima?">
        <i class="far fa-envelope"></i> Kirim
    </button>
</div>
<button type="reset" class="btn btn-default" data-alert-type="warning"
    data-alert-text="Apakah Anda yakin surat ini dihapus?">
    <i class="far fa-trash-alt"></i> Buang
</button>