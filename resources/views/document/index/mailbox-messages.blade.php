<div class="table-responsive mailbox-messages">
    <table class="table table-hover table-striped">
        <tbody>
            <tr>
                <td>
                    <div class="icheck-primary">
                        <input type="checkbox" value="" id="check1">
                        <label for="check1"></label>
                    </div>
                </td>
                <td class="mailbox-star"><a href="#"><i class="far fa-comment-dots text-info"></i></a></td>
                <td class="mailbox-name"><a href="#">Divisi Business Enabler & ICT</a></td>
                <td class="mailbox-subject"><b>Pembahasan PP/UU ITE terkait Kras Mart & Dokumen & Materai elektronik</b>
                </td>
                <td class="mailbox-attachment"></td>
                <td class="mailbox-date">5 mins ago</td>
            </tr>
            <tr>
                <td>
                    <div class="icheck-primary">
                        <input type="checkbox" value="" id="check2">
                        <label for="check2"></label>
                    </div>
                </td>
                <td class="mailbox-star"><a href="#"><i class="fas fa-exclamation text-warning"></i></a></td>
                <td class="mailbox-name"><a href="#">Divisi Quality Assurance</a></td>
                <td class="mailbox-subject"><b>Pelaksanaan 5R Melalui Kegiatan Jumat Bersih</b>
                </td>
                <td class="mailbox-attachment"><i class="fas fa-paperclip"></i></td>
                <td class="mailbox-date">28 mins ago</td>
            </tr>
            <tr>
                <td>
                    <div class="icheck-primary">
                        <input type="checkbox" value="" id="check3">
                        <label for="check3"></label>
                    </div>
                </td>
                <td class="mailbox-star"><a href="#"><i class="fas fa-lock text-danger"></i></a></td>
                <td class="mailbox-name"><a href="#">Divisi Quality Assurance</a></td>
                <td class="mailbox-subject"><b>Pergantian dan Penambahan User Release Reservasi Divisi PPC</b>
                </td>
                <td class="mailbox-attachment"><i class="fas fa-paperclip"></i></td>
                <td class="mailbox-date">28 mins ago</td>
            </tr>
        </tbody>
    </table>
    <!-- /.table -->
</div>
<!-- /.mail-box-messages -->