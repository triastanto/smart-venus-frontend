<h3 class="card-title">Inbox</h3>

<div class="card-tools">
    <div class="input-group input-group-sm">
        <input type="text" class="form-control" placeholder="Search Mail">
        <div class="input-group-append">
            <div class="btn btn-primary">
                <i class="fas fa-search"></i>
            </div>
        </div>
    </div>
</div>
<!-- /.card-tools -->