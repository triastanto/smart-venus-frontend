{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content')
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">
        <a href="" class="btn btn-primary btn-block mb-3">Back to Inbox</a>
        @include('layouts.folders')
        @include('layouts.labels')
      </div>
      <!-- /.col -->
      <div class="col-md-9">
        <div class="card card-primary card-outline">
          <div class="card-header">
            @include('document.show.card-header')
          </div>
          <!-- /.card-header -->
          <div class="card-body p-0">
            <div class="mailbox-read-info">
              @include('document.show.mailbox-read-info')
            </div>
            <!-- /.mailbox-read-info -->
            <div class="mailbox-controls with-border text-center">
              @include('document.show.mailbox-controls')
            </div>
            <!-- /.mailbox-controls -->
            <div class="mailbox-read-message">
              @include('document.show.mailbox-read-message')
            </div>
            <!-- /.mailbox-read-message -->
          </div>
          <!-- /.card-body -->
          <div class="card-footer bg-white">
            @include('document.show.mailbox-attachments')
          </div>
          <!-- /.card-footer -->
          <div class="card-footer">
            @include('document.show.card-footer')
          </div>
          <!-- /.card-footer -->
        </div>
        <!-- /.card -->
        <div class="card card-primary card-outline">
          @include('document.show.workflow-progress')
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
</section>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('footer')
<!-- To the right -->
<div class="float-right d-none d-sm-inline">
  Anything you want
</div>
<!-- Default to the left -->
<strong>Copyright © 2014-2019 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
@stop

@section('js')
<script src="/js/show.js"></script>
@stop