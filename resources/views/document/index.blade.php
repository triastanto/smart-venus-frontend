{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content')
<section class="content">
    <div class="row">
      <div class="col-md-3">
        <a href="" class="btn btn-primary btn-block mb-3">Compose</a>
        @include('layouts.folders')
        @include('layouts.labels')
      </div>
      <!-- /.col -->
      <div class="col-md-9">
        <div class="card card-primary card-outline">
          <div class="card-header">
            @include('document.index.card-header')
          </div>
          <!-- /.card-header -->
          <div class="card-body p-0">
            @include('document.index.mailbox-controls')
            @include('document.index.mailbox-messages')
          </div>
          <!-- /.card-body -->
          <div class="card-footer p-0">
            @include('document.index.mailbox-controls')
          </div>
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('footer')
  @include('layouts.footer')
@stop

@section('js')
<script src="/js/index.js"></script>
@stop