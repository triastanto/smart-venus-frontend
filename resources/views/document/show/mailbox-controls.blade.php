<div class="btn-group">
    <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Delete">
        <i class="far fa-trash-alt"></i></button>
    <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Reply">
        <i class="fas fa-reply"></i></button>
    <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Forward">
        <i class="fas fa-share"></i></button>
</div>
<!-- /.btn-group -->
<button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" title="Print">
    <i class="fas fa-print"></i></button>