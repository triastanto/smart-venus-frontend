{{-- <ul class="mailbox-attachments d-flex align-items-stretch clearfix">
    <li>
        <span class="mailbox-attachment-icon"><i class="far fa-file-pdf"></i></span>

        <div class="mailbox-attachment-info">
            <a href="#" class="mailbox-attachment-name"><i class="fas fa-paperclip"></i> Sep2014-report.pdf</a>
            <span class="mailbox-attachment-size clearfix mt-1">
                <span>1,245 KB</span>
                <a href="#" class="btn btn-default btn-sm float-right"><i class="fas fa-cloud-download-alt"></i></a>
            </span>
        </div>
    </li>
    <li>
        <span class="mailbox-attachment-icon"><i class="far fa-file-word"></i></span>

        <div class="mailbox-attachment-info">
            <a href="#" class="mailbox-attachment-name"><i class="fas fa-paperclip"></i> App Description.docx</a>
            <span class="mailbox-attachment-size clearfix mt-1">
                <span>1,245 KB</span>
                <a href="#" class="btn btn-default btn-sm float-right"><i class="fas fa-cloud-download-alt"></i></a>
            </span>
        </div>
    </li>
    <li>
        <span class="mailbox-attachment-icon has-img"><img src="../../dist/img/photo1.png" alt="Attachment"></span>

        <div class="mailbox-attachment-info">
            <a href="#" class="mailbox-attachment-name"><i class="fas fa-camera"></i> photo1.png</a>
            <span class="mailbox-attachment-size clearfix mt-1">
                <span>2.67 MB</span>
                <a href="#" class="btn btn-default btn-sm float-right"><i class="fas fa-cloud-download-alt"></i></a>
            </span>
        </div>
    </li>
    <li>
        <span class="mailbox-attachment-icon has-img"><img src="../../dist/img/photo2.png" alt="Attachment"></span>

        <div class="mailbox-attachment-info">
            <a href="#" class="mailbox-attachment-name"><i class="fas fa-camera"></i> photo2.png</a>
            <span class="mailbox-attachment-size clearfix mt-1">
                <span>1.9 MB</span>
                <a href="#" class="btn btn-default btn-sm float-right"><i class="fas fa-cloud-download-alt"></i></a>
            </span>
        </div>
    </li>
</ul> --}}
<table class="table">
    <thead>
      <tr>
        <th>File Name</th>
        <th>File Size</th>
        <th></th>
      </tr>
    </thead>
    <tbody>

      <tr>
        <td>Functional-requirements.docx</td>
        <td>49.8005 kb</td>
        <td class="text-right py-0 align-middle">
          <div class="btn-group btn-group-sm">
            <a href="#" class="btn btn-info"><i class="fas fa-eye"></i></a>
            <a href="#" class="btn btn-danger"><i class="fas fa-trash"></i></a>
          </div>
        </td>
      <tr>
        <td>UAT.pdf</td>
        <td>28.4883 kb</td>
        <td class="text-right py-0 align-middle">
          <div class="btn-group btn-group-sm">
            <a href="#" class="btn btn-info"><i class="fas fa-eye"></i></a>
            <a href="#" class="btn btn-danger"><i class="fas fa-trash"></i></a>
          </div>
        </td>
      <tr>
        <td>Email-from-flatbal.mln</td>
        <td>57.9003 kb</td>
        <td class="text-right py-0 align-middle">
          <div class="btn-group btn-group-sm">
            <a href="#" class="btn btn-info"><i class="fas fa-eye"></i></a>
            <a href="#" class="btn btn-danger"><i class="fas fa-trash"></i></a>
          </div>
        </td>
      <tr>
        <td>Logo.png</td>
        <td>50.5190 kb</td>
        <td class="text-right py-0 align-middle">
          <div class="btn-group btn-group-sm">
            <a href="#" class="btn btn-info"><i class="fas fa-eye"></i></a>
            <a href="#" class="btn btn-danger"><i class="fas fa-trash"></i></a>
          </div>
        </td>
      <tr>
        <td>Contract-10_12_2014.docx</td>
        <td>44.9715 kb</td>
        <td class="text-right py-0 align-middle">
          <div class="btn-group btn-group-sm">
            <a href="#" class="btn btn-info"><i class="fas fa-eye"></i></a>
            <a href="#" class="btn btn-danger"><i class="fas fa-trash"></i></a>
          </div>
        </td>

    </tbody>
  </table>