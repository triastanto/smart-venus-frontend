{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content')
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">
        <a href="" class="btn btn-primary btn-block mb-3">Back to Inbox</a>
        @include('layouts.folders')
        @include('layouts.labels')
      </div>
      <!-- /.col -->
      <div class="col-md-9">
        <div class="card card-primary card-outline">
          <div class="card-header">
            <h3 class="card-title">Tulis Surat Baru</h3>
          </div>
          <!-- /.card-header -->
          <form action="" method="post">
            <div class="card-body">
              @include('document.create.to-subject')
              @include('document.create.compose-textarea')
              @include('document.create.initializer')
              @include('document.create.attachment')
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              @include('document.create.card-footer')
            </div>
            <!-- /.card-footer -->
          </form>
          <!-- /form -->
        </div>
        <!-- /.card -->

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('footer')
@include('layouts.footer')
@stop

@section('js')
<script src="/js/create.js"></script>
@stop