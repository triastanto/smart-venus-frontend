<div class="card">
    <div class="card-header">
        <h3 class="card-title">Progres Surat</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="card-body p-0">
        <ul class="nav nav-pills flex-column">
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="far fa-circle text-primary"></i>
                    Disposisi
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="far fa-circle text-info"></i>
                    Selesai
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="far fa-circle text-danger"></i>
                    Ditolak
                </a>
            </li>
        </ul>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->