$(document).ready(function () {
    // multiple file upload
    bsCustomFileInput.init();

    // WYSYWIG editor
    $('#compose-textarea').summernote({
        placeholder: 'Hello stand alone ui',
        tabsize: 2,
        height: 400
    });

    // multiple selection
    $('.select2').select2();

    // datepicker
    $('input[name="tanggal"]').daterangepicker({
        autoUpdateInput: false,
        singleDatePicker: true,
        locale: {
            format: "DD/MM/YYYY",
            separator: " - ",
            applyLabel: "Apply",
            cancelLabel: "Cancel",
            fromLabel: "From",
            toLabel: "To",
            customRangeLabel: "Custom",
            weekLabel: "W",
            daysOfWeek: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
            monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            firstDay: 1
        }
    });
    $('input[name="tanggal"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY'));
    });
    $('input[name="tanggal"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });

    // button click handler
    $('body').on('click', 'button', function (e) {
        e.preventDefault();
        const form = $(this).parents('form:first');
        const alertType = $(this).data('alertType');
        const alertText = $(this).data('alertText');
        Swal.fire({ type: alertType, text: alertText, showCancelButton: true,
        }).then((result) => {
            if (result.value) { 
                // form.submit();
                Swal.fire( {text: 'Alert confirmed!', type: 'success'}) 
            }
        });
    });
});